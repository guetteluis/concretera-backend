const env = require('./config/env');
const express = require('express');
const helmet = require('helmet');
const compression = require('compression');
const mongoose = require('mongoose');
const database = require('./config/database');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./app/Http/routes');
const SerialPort = require('serialport');

// .env file configuration
env.get();

// Express initialization
const app = express();
let http = require('http').Server(app);
let io = require('socket.io')(http);

// Helmet initialization
app.use(helmet());

// compress all responses
app.use(compression());

// MongoDB connection
mongoose.connect(database.mongodb.uri, {
    useMongoClient: true,
    user: database.mongodb.username,
    pass: database.mongodb.password
});
mongoose.Promise = global.Promise;

// On connection error
mongoose.connection.on('error', (error) => {
    console.log('Database error: ' + error);
});

// On successful connection
mongoose.connection.on('connected', () => {
    console.log('Connected to database');
});

// CORS initialization
app.use(cors());

// Body parser middleware
app.use(bodyParser.json());

// Routes
app.use('/', routes);

const serialPort = new SerialPort(process.env.SERIAL_PORT, {
    baudRate: 9600,
});

function write(message) {
    serialPort.write(message, function(err) {
        if (err) {
            return console.log('Error on write: ', err.message);
        }
        console.log('message written');
    });

// Open errors will be emitted as an error event
    serialPort.on('error', function(err) {
        console.log('Error: ', err.message);
    })
}

function read() {
    const Readline = SerialPort.parsers.Readline;
    const parser = new Readline();
    serialPort.pipe(parser);

    serialPort.on('open', (error) => {
        console.log('open connection');
    });

    parser.on('data', (data) => {
        io.emit('status', data);
    });
}

io.on('connection', (socket) => {

    read();

    socket.on('start', function(message){
        write(message);
        console.log(message);
    });

    socket.on('pause', function(message){
        write(message);
        console.log(message);
    });

    socket.on('manual', function(message){
        write(message);
        console.log(message);
    });

    socket.on('stop', function(message){
        write(message);
        console.log(message);
    });

});

const port = 3001;

http.listen(port, function(){
    console.log('listening in http://localhost:' + port);
});

const server = app.listen(process.env.PORT || 8080, () => {
    const port = server.address().port;
    console.log('app running on port', port);
});