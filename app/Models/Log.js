const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');

const LogSchema = new Schema({
    _user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    arena: {
        type: Number,
        required: true
    },
    cemento: {
        type: Number,
        required: true
    },
    piedra: {
        type: Number,
        required: true
    },
    agua_aditivo: {
        type: Number,
        required: true
    },
    agua: {
        type: Number,
        required: true
    },
}, {timestamps: true, _id: true});

LogSchema.plugin(uniqueValidator);
const Log = mongoose.model('Log', LogSchema);

module.exports = Log;

module.exports.genPasswordHash = (password) => {
    let salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
};

module.exports.comparePassword = (candidatePassword, hash) => {
    return bcrypt.compareSync(candidatePassword, hash);
};