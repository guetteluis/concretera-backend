const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
const mongooseHidden = require('mongoose-hidden')();

const UserSchema = new Schema({
    username: {
        type: String,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        hide: true
    },
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['admin', 'oparator', 'guest'],
        default: 'guest'
    }
}, {timestamps: true, _id: true});

UserSchema.plugin(mongooseHidden, { hidden: { _id: false } });
UserSchema.plugin(uniqueValidator);
const User = mongoose.model('User', UserSchema);

module.exports = User;

module.exports.genPasswordHash = (password) => {
    let salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
};

module.exports.comparePassword = (candidatePassword, hash) => {
    return bcrypt.compareSync(candidatePassword, hash);
};