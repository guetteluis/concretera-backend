const SerialPort = require('serialport');
const env = require('../../../config/env');


exports.reading = () => {
    env.get();
    const port = new SerialPort(process.env.SERIAL_PORT, {
        baudRate: 9600,
    });
    const Readline = SerialPort.parsers.Readline;
    const parser = new Readline();
    port.pipe(parser);

    port.on('open', (error) => {
        console.log('open connection');
    });

    parser.on('data', (data) => {
        io.emit()
    })
};
