const User = require('../../Models/User');
const jwt = require('jsonwebtoken');
const jwtConfig = require('../../../config/jwt');

exports.register = function (request, response, next) {
    let newUser = new User(request.body);

    newUser.password = User.genPasswordHash(newUser.password);

    newUser.save(function (error) {
        if (error) {
            if (error.errors && error.errors.email) {
                return response.status(500).send({
                    success: false,
                    message: 'email_already_used'
                });
            }

            return response.status(500).send({
                success: false,
                message: 'user_not_created'
            });
        }

        return response.send({
            success: true,
            message: 'user_created'
        });
    });
};

exports.login = function (request, response, next) {
    let data = request.body;

    User.findOne({username: data.username}, function (error, user) {
        if (error || !user) {
            return response.status(500).send({
                success: false,
                message: 'user_not_found'
            });
        }

        if (User.comparePassword(data.password, user.password)) {
            let token = jwt.sign({ data: user }, jwtConfig.secret, {
                expiresIn: jwtConfig.expireTime
            });

            response.send({
                success: true,
                message: 'user_logged_in',
                data : {
                    token: 'Bearer ' + token,
                    user: user
                }
            });
        } else {
            return response.status(500).send({
                success: false,
                message: 'wrong_password'
            });
        }
    });
};