const Log = require('../../Models/Log');

exports.create = (request, response, next) => {
    let log = new Log(request.body);

    log.save((error) => {
        if (error) {
            return response.status(500).send({
                success: false,
                message: 'log_not_created'
            });
        }
        return response.send({
            success: true,
            message: 'log_created'
        });
    });
};

exports.all = (request, response, next) => {
    Log.find().populate('_user').exec((error, logs) => {
        if (error) {
            return response.status(500).send({
                success: false,
                message: 'server_error'
            });
        }
        return response.send({
            success: true,
            data: logs
        });
    });
};