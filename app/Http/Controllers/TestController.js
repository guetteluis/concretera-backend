const SerialPort = require('serialport');
const env = require('../../../config/env');


exports.test = (request, response, next) => {
    env.get();
    const port = new SerialPort(process.env.SERIAL_PORT, {
        baudRate: 9600,
    });
    const Readline = SerialPort.parsers.Readline;
    const parser = new Readline();
    port.pipe(parser);

    const message = '#13ON';

    port.on('open', (error) => {
        console.log('open connection');
    });

    parser.on('data', (data) => {
        console.log(data);
    })
};
