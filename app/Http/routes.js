const express = require('express');
const router = express.Router();
const TestController = require('./Controllers/TestController');
const AuthController = require('./Controllers/AuthController');
const LogController = require('./Controllers/LogController');

router.get('/', TestController.test);

router.post('/register', AuthController.register);
router.post('/login', AuthController.login);

router.post('/logs', LogController.create);
router.get('/logs', LogController.all);

module.exports = router;