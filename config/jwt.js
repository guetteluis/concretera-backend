module.exports = {
    secret: 'concretera-typeiqs',
    expireTime: 604800,
    registrationExpireTime: 50000
};