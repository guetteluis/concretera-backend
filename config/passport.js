const Jwt = require('passport-jwt');
const User = require('../app/Models/User');
const jwtConfig = require('./jwt');

const JwtStrategy = Jwt.Strategy;
const ExtractJwt = Jwt.ExtractJwt;

module.exports = function (passport) {
    let options = {};
    options.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    options.secretOrKey = jwtConfig.secret;

    passport.use(new JwtStrategy(options, function(jwt_payload, done) {
        User.findOne({id: jwt_payload.sub}, function(err, user) {
            if (err) {
                return done(err, false);
            }
            if (user) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        });
    }));
};